package sample.caique.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

open class BaseRecyclerViewAdapter<T>(
    private var items: List<T> = listOf(),
    @LayoutRes val layoutResId: Int,
    private val bindView: BaseRecyclerViewAdapter<T>.(view: View, item: T) -> Unit
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder<T>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<T> {
        val viewItem = LayoutInflater.from(parent.context).inflate(layoutResId, parent, false)
        return BaseViewHolder(this, viewItem, bindView)
    }

    override fun getItemCount() = this.items.size

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) {
        holder.bindView(getItem(position))
    }

    fun setItems(items: List<T>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun getItems() = items

    fun getItem(position: Int) = items[position]
}

class BaseViewHolder<T>(
    private val baseRecyclerViewAdapter: BaseRecyclerViewAdapter<T>,
    val view: View,
    private val executeBinding: BaseRecyclerViewAdapter<T>.(view: View, item: T) -> Unit
) :
    androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {

    fun bindView(item: T) {
        this.baseRecyclerViewAdapter.executeBinding(view, item)
    }
}