package sample.caique.common

import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers


/**
 * Just a transformer to a better order of emissions at view
 * This transformer will emmit (success scenario) : start, Resul, End
 * This transformer will emmit (error scenario) : start, Failed
 */

sealed class Event<out T>

object Start : Event<Nothing>()
data class Failed(val reason: Throwable) : Event<Nothing>()
data class Result<out T>(val value: T) : Event<T>()
object End : Event<Nothing>()

class UiState<T>(private val uiScheduler: Scheduler = Schedulers.trampoline()) :
    ObservableTransformer<T, Event<T>> {

    override fun apply(upstream: Observable<T>): Observable<Event<T>> {

        val end = Observable.just(End)

        return upstream
            .map { value: T -> Result(value) as Event<T> }
            .onErrorReturn { error: Throwable -> Failed(error) }
            .startWith(Start)
            .concatWith(end)
            .observeOn(uiScheduler)
    }

}