package sample.caique.common

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.error_view.view.*

class GenericErrorView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : LinearLayout(context, attrs, defStyleAttr, defStyleRes) {

    init {
        View.inflate(context, R.layout.error_view, this)
    }

    fun retry(onClick: () -> Unit) {
        errorStateActionButton.isClickable = true
        errorStateActionButton.setOnClickListener {
            onClick()
            errorStateActionButton.isClickable = false
        }
    }
}