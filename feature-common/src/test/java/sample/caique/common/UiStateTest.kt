package sample.caique.common

import io.reactivex.Observable
import labs.dotanuki.tite.checks.broken
import labs.dotanuki.tite.checks.completed
import labs.dotanuki.tite.given
import org.junit.Test
import sample.caique.domain.error.HttpIntegrationError

class UiStateTest {

    @Test
    fun `given a event should emmit a sequence of ui state, success scenario`() {
        val observable = Observable.just("item")
            .compose(UiState())

        given(observable) {

            assertThatSequence {
                should be completed
                should notBe broken
            }

            verifyForEmissions {
                items are listOf(Start, Result("item"), End)
            }
        }
    }

    @Test
    fun `given a event should emmit a sequence of ui state, error scenario`() {
        val observable = Observable.error<String>(HttpIntegrationError.ClientIssue)
            .compose(UiState())

        given(observable) {

            assertThatSequence {
                should be completed
                should notBe broken
            }

            verifyForEmissions {
                items are listOf(Start, Failed(HttpIntegrationError.ClientIssue), End)
            }
        }
    }

}