package sample.caique.cache.di

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton
import sample.caique.cache.InMemoryCache
import sample.caique.domain.service.CacheService

val cacheModule = Kodein.Module(name = "cache") {

    bind<CacheService>() with singleton {
        InMemoryCache()
    }
}