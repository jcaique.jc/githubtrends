package sample.caique.cache

import sample.caique.domain.CacheKey
import sample.caique.domain.service.CacheService

class InMemoryCache : CacheService {

    private var cache = mutableMapOf<CacheKey, Any>()
    override fun clear() {
        cache.clear()
    }

    override fun remove(key: CacheKey) {
        cache.remove(key)
    }

    override fun <T> store(key: CacheKey, value: T) {
        cache[key] = value as Any
    }

    override fun <T> retrieve(key: CacheKey): T? {
        return cache[key] as T?
    }
}