# githubtrends

Just an Over Engineering project about github trends

## Building and Running

### Running from IDE

- Ensure you have Android Studio 3.2.1 or newer
- Is recommend to install Kotlinx.Serialization plugin on your IDE ([instructions](https://github.com/Kotlin/kotlinx.serialization))

### Building from CLI

To run all unit tests and build a APK, execute

```
./gradlew build
```

### Running integration tests

To run acceptance tests powered by Instrumentation + Espresso, execute

```
./gradlew connectedCheck
```
## Knowledge Stack

This project leverages on

- Kotlin
- RxJava2 for end-to-end reactive programming
- Kodein for Dependency Injection
- Kotlinx.Serialization for automatic JSON handling
- OkHttp3 + Retrofit for networking over HTTP
- Several Multi-module project


