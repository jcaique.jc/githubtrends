package sample.caique.scheduler

object SchedulerTag {

    const val uiScheduler = "AndroidScheduler"
    const val ioScheduler = "IOScheduler"
}
