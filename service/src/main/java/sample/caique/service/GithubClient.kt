package sample.caique.service

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import sample.caique.service.response.Issues
import sample.caique.service.response.Repository

interface GithubClient {

    @Headers("Accept: application/vnd.github.mercy-preview+json")
    @GET("search/repositories?q=android%20language:kotlin&sort=stars&order=desc&per_page=10")
    fun retrieveTrends(): Observable<Repository>


    @Headers("Accept: application/vnd.github.mercy-preview+json")
    @GET("repos/{user}/{repo}/issues")
    fun retrieveIssues(@Path("user") user: String, @Path("repo") repo: String): Observable<ArrayList<Issues>>

}