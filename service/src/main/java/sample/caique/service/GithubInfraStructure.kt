package sample.caique.service

import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import sample.caique.domain.RepositoryOverview
import sample.caique.domain.service.GithubService
import sample.caique.logger.Logger

class GithubInfraStructure(
    private val api: GithubClient,
    private val logger: Logger,
    private val scheduler: Scheduler = Schedulers.trampoline()
) : GithubService {

    private val execution by lazy {
        InfrastructureExecution<List<RepositoryOverview>>(scheduler, logger)
    }

    override fun retrieve(): Observable<List<RepositoryOverview>> {
        return api.retrieveTrends()
            .map { RepositoryMapper(it) }
            .compose(execution)
    }
}