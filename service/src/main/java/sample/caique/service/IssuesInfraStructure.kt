package sample.caique.service

import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import sample.caique.domain.Issue
import sample.caique.domain.service.IssueService
import sample.caique.logger.Logger

class IssuesInfraStructure(
    private val api: GithubClient,
    private val logger: Logger,
    private val scheduler: Scheduler = Schedulers.trampoline()
) : IssueService {

    private val execution by lazy {
        InfrastructureExecution<List<Issue>>(scheduler, logger)
    }

    override fun retrieveIssues(fullName: String): Observable<List<Issue>> {
        val split = fullName.split("/")
        return api.retrieveIssues(split[0], split[1])
            .map { IssueMapper(it) }
            .compose(execution)
    }
}