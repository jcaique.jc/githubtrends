package sample.caique.service

import sample.caique.domain.Issue
import sample.caique.service.response.Issues

object IssueMapper {

    operator fun invoke(issues: List<Issues>): List<Issue> {
        return issues.map {
            Issue(title = it.title, comments = it.comments, body = it.body ?: "No description")
        }
    }
}