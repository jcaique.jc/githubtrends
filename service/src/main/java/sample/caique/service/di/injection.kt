package sample.caique.service.di

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import sample.caique.di.networkingModule
import sample.caique.domain.service.GithubService
import sample.caique.domain.service.IssueService
import sample.caique.scheduler.SchedulerTag
import sample.caique.service.GithubClient
import sample.caique.service.GithubInfraStructure
import sample.caique.service.IssuesInfraStructure

val serviceModule = Kodein.Module(name = "service") {

    importOnce(networkingModule)

    bind<GithubClient>() with singleton {
        val retrofit: Retrofit = instance()
        retrofit.create(GithubClient::class.java)
    }

    bind<GithubService>() with provider {
        GithubInfraStructure(
            scheduler = instance(SchedulerTag.ioScheduler),
            logger = instance(),
            api = instance()
        )
    }

    bind<IssueService>() with provider {
        IssuesInfraStructure(
            scheduler = instance(SchedulerTag.ioScheduler),
            logger = instance(),
            api = instance()
        )
    }

}