package sample.caique.service

import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import sample.caique.logger.Logger
import sample.caique.networking.HandleConnectivityIssue
import sample.caique.networking.HandleErrorByHttpStatus
import sample.caique.networking.HandleSerializationError

class InfrastructureExecution<T>(
    private val scheduler: Scheduler = Schedulers.trampoline(),
    private val logger: Logger
) : ObservableTransformer<T, T> {

    override fun apply(upstream: Observable<T>): ObservableSource<T> {
        return upstream
            .subscribeOn(scheduler)
            .compose(HandleErrorByHttpStatus())
            .compose(HandleConnectivityIssue())
            .compose(HandleSerializationError())
            .doOnError { logger.e("api error -> Failed with $it") }
            .doOnNext { logger.v("api success -> Success") }
    }
}