package sample.caique.service

import io.dotanuki.burster.using
import labs.dotanuki.tite.checks.broken
import labs.dotanuki.tite.checks.completed
import labs.dotanuki.tite.checks.terminated
import labs.dotanuki.tite.given
import org.junit.Test
import sample.caique.domain.error.GithubIntegrationError.UnexpectedResponse
import sample.caique.service.response.Item
import sample.caique.service.response.Owner
import sample.caique.service.response.Repository

internal typealias JsonFile = String

class GithubInfraStructureTest {


    private val repo = Repository(
        items = listOf(
            Item(
                id = 7190986,
                description = "A shadowsocks client for Android",
                url = "https://api.github.com/repos/shadowsocks/shadowsocks-android",
                score = 72.74426,
                owner = Owner("https://avatars1.githubusercontent.com/u/3006190?v=4"),
                name = "shadowsocks/shadowsocks-android",
                htmlUrl = "https://github.com/shadowsocks/shadowsocks-android",
                watcher = 19046,
                stars = 19046,
                forks = 7907
            )
        )
    )

    @Test
    fun `should integrate with github, success scenario`() {

        using<JsonFile, Repository> {

            burst {
                values("repositories.json", repo)
            }

            thenWith { filePath, _ ->
                val setup = TestSetup()
                val service = setup.service

                val execution = service.retrieve()

                setup.defineScenario(
                    statusCode = 200,
                    path = filePath
                )

                given(execution) {
                    assertThatSequence {
                        should be completed
                        should notBe broken
                        should be terminated
                    }
                }
            }
        }
    }


    @Test
    fun `should integrate with github, error scenario`() {

        using<JsonFile, Repository> {

            burst {
                values("repositories_invalid.json", repo)
            }

            thenWith { filePath, _ ->
                val setup = TestSetup()
                val service = setup.service

                val execution = service.retrieve()

                setup.defineScenario(
                    statusCode = 200,
                    path = filePath
                )

                given(execution) {
                    assertThatSequence {
                        should be broken
                    }

                    verifyWhenError {
                        fails byError UnexpectedResponse
                    }
                }
            }
        }
    }

}