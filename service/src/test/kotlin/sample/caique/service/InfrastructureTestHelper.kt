package sample.caique.service

import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import sample.caique.domain.service.GithubService
import sample.caique.logger.ConsoleLogger
import sample.caique.networking.BuildRetrofit

internal class TestSetup {

    val server: MockWebServer = MockWebServer()
    val service: GithubService

    init {

        val url = server.url("/").toString()
        val api = BuildRetrofit(url, OkHttpClient()).create(GithubClient::class.java)

        service = GithubInfraStructure(
            api = api,
            logger = ConsoleLogger
        )
    }

    fun defineScenario(statusCode: Int, path: String) {
        val json = loadFile(path)

        server.enqueue(
            MockResponse().apply {
                setResponseCode(statusCode)
                setBody(json)
            }
        )
    }
}