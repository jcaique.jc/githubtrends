package sample.caique.service

import io.dotanuki.burster.using
import labs.dotanuki.tite.checks.broken
import labs.dotanuki.tite.checks.completed
import labs.dotanuki.tite.checks.terminated
import labs.dotanuki.tite.given
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Test
import sample.caique.domain.Issue
import sample.caique.domain.service.IssueService
import sample.caique.logger.ConsoleLogger
import sample.caique.networking.BuildRetrofit

class IssuesInfraStructureTest {


    private val issue = listOf(
        Issue(
            comments = 1,
            title = "Nexus 5 official ROM Android 5.1.1 Crash",
            body = "a body description"
        )
    )

    @Test
    fun `should integrate with github issues, success scenario`() {

        using<JsonFile, List<Issue>> {

            burst {
                values("issues.json", issue)
            }

            thenWith { filePath, _ ->
                val setup = IssueTestSetup()
                val service = setup.service

                val execution = service.retrieveIssues("repo/name")

                setup.defineScenario(
                    statusCode = 200,
                    path = filePath
                )

                given(execution) {
                    assertThatSequence {
                        should be completed
                        should notBe broken
                        should be terminated
                    }
                }
            }
        }
    }

}

class IssueTestSetup {

    val server: MockWebServer = MockWebServer()
    val service: IssueService

    init {

        val url = server.url("/").toString()
        val api = BuildRetrofit(url, OkHttpClient()).create(GithubClient::class.java)

        service = IssuesInfraStructure(
            api = api,
            logger = ConsoleLogger
        )
    }

    fun defineScenario(statusCode: Int, path: String) {
        val json = loadFile(path)

        server.enqueue(
            MockResponse().apply {
                setResponseCode(statusCode)
                setBody(json)
            }
        )
    }
}