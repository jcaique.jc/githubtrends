package sample.caique.logger

object TraceInspector {

    private const val grandFatherIndex = 2

    fun findClassName(): String {
        val stackTraceElement = Throwable().stackTrace[grandFatherIndex]
        return stackTraceElement.className
            .split(".").last()  // Strip simple name from full.package.name
            .split("$").first() // Strip name when caller$inside$lambdas
    }

}