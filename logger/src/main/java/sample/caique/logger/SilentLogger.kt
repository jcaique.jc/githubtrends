package sample.caique.logger

object SilentLogger : Logger {

    override fun v(message: String) = Unit

    override fun d(message: String) = Unit

    override fun i(message: String) = Unit

    override fun w(message: String) = Unit

    override fun e(message: String) = Unit

}