package sample.caique.details

import kotlinx.android.synthetic.main.item_detail.view.*
import sample.caique.common.BaseRecyclerViewAdapter
import sample.caique.domain.Issue

class IssuesAdapter :
    BaseRecyclerViewAdapter<Issue>(layoutResId = R.layout.item_detail, bindView = { view, item ->
        with(item) {
            view.title.text = title
            view.description.text = body.trim()
            view.comments.text = comments.toString()
        }
    })