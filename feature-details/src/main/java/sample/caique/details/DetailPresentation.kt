package sample.caique.details

import sample.caique.domain.Issue

data class DetailPresentation(
    val name: String,
    val photoUrl: String,
    val description: String,
    val issues: List<Issue>
)