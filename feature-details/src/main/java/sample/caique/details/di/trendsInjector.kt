package sample.caique.details.di

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import sample.caique.common.UiState
import sample.caique.details.DetailsViewModel
import sample.caique.scheduler.SchedulerTag
import sample.caique.trends.DetailViewModelFactory

val details = Kodein.Module(name = "details") {

    bind() from provider {

        val factory = DetailViewModelFactory(
            service = instance(),
            state = UiState(uiScheduler = instance(SchedulerTag.uiScheduler)),
            cache = instance()
        )

        val activity: FragmentActivity = instance()

        ViewModelProviders.of(activity, factory)
            .get(DetailsViewModel::class.java)
    }
}