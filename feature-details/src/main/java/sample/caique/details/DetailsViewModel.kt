package sample.caique.details

import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import sample.caique.common.Event
import sample.caique.common.UiState
import sample.caique.domain.Detail
import sample.caique.domain.RepositoryOverview
import sample.caique.domain.Trends
import sample.caique.domain.error.NotFoundError
import sample.caique.domain.service.CacheService
import sample.caique.domain.service.IssueService

/**
 * Retrieve informations from inmemory cache and performe request to recovery issues
 */

class DetailsViewModel(
    private val api: IssueService,
    private val state: UiState<DetailPresentation>,
    private val cache: CacheService
) : ViewModel() {

    fun retrieveDetail(): Observable<Event<DetailPresentation>> {
        val key = cache.retrieve<Int>(Detail)

        val detail = cache.retrieve<List<RepositoryOverview>>(Trends)?.filter { it.id == key }?.firstOrNull()
        val issues = detail?.let { Observable.just(detail) } ?: Observable.error<RepositoryOverview>(NotFoundError)
        return issues
            .flatMap { api.retrieveIssues(detail!!.name) }
            .map { DetailMapper(detail!!, it) }
            .compose(state)
    }
}