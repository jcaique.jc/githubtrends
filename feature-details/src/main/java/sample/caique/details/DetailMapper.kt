package sample.caique.details

import sample.caique.domain.Issue
import sample.caique.domain.RepositoryOverview

object DetailMapper {

    operator fun invoke(repo: RepositoryOverview, issue: List<Issue>): DetailPresentation {
        return DetailPresentation(
            name = repo.name,
            photoUrl = repo.imgUrl,
            description = repo.description,
            issues = issue
        )
    }
}