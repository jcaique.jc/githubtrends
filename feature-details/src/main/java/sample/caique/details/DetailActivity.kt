package sample.caique.details

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.activity_detail.*
import org.kodein.di.direct
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import sample.caique.common.End
import sample.caique.common.Failed
import sample.caique.common.Result
import sample.caique.common.Start
import sample.caique.injection.ProvidesInjector

class DetailActivity : AppCompatActivity() {

    private val graph by lazy {
        val injector = (application as ProvidesInjector).injector

        injector.reconfigure {
            bind<FragmentActivity>() with provider { this@DetailActivity }
        }

        injector.dependencies().direct
    }

    private val vm by lazy { graph.instance<DetailsViewModel>() }

    private val disposable by lazy {
        CompositeDisposable()
    }

    private val adapter by lazy {
        IssuesAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        with(issues) {
            layoutManager = LinearLayoutManager(this@DetailActivity)
        }
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { finish() }

        details()
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }

    private fun details() {
        disposable += vm.retrieveDetail()
            .subscribe(
                {
                    when (it) {
                        is Start -> start()
                        is Result -> setupView(it.value)
                        is Failed -> failed()
                        is End -> skeleton.visibility = View.GONE
                    }
                },
                { e -> e.printStackTrace() })

    }

    private fun start() {
        skeleton.visibility = View.VISIBLE
        errorState.visibility = View.GONE
        issues.visibility = View.VISIBLE
    }

    private fun setupView(value: DetailPresentation) {
        adapter.setItems(value.issues)
        issues.adapter = adapter
        toolbar.title = value.name
        toolbar.subtitle = "Issues"
        issues.visibility = View.VISIBLE
    }

    private fun failed() {
        errorState.visibility = View.VISIBLE
        errorState.setOnClickListener { details() }
        issues.visibility = View.GONE
    }
}