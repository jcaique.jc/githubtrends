package sample.caique.trends

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import sample.caique.common.UiState
import sample.caique.details.DetailPresentation
import sample.caique.details.DetailsViewModel
import sample.caique.domain.service.CacheService
import sample.caique.domain.service.IssueService

class DetailViewModelFactory(
    private val service: IssueService,
    private val state: UiState<DetailPresentation>,
    private val cache: CacheService
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DetailsViewModel(service, state, cache) as T
    }
}