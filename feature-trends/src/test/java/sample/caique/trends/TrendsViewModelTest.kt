package sample.caique.trends

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import labs.dotanuki.tite.given
import org.junit.Before
import org.junit.Test
import sample.caique.common.End
import sample.caique.common.Result
import sample.caique.common.Start
import sample.caique.common.UiState
import sample.caique.domain.Navigator
import sample.caique.domain.RepositoryOverview
import sample.caique.domain.service.CacheService
import sample.caique.domain.service.GithubService

class TrendsViewModelTest {

    private val api: GithubService = mock()
    private val state: UiState<List<RepositoryOverview>> = UiState()
    private val cache: CacheService = mock()
    private val navigate: Navigator = mock()

    private lateinit var vm: TrendsViewModel
    @Before
    fun `before each test`() {
        vm = TrendsViewModel(api, state, cache, navigate)
    }

    @Test
    fun `given a sucess request should save on cache`() {

        `trends for new request`()
        `when interact with cache`()

        val uiEvents = sequenceOf(Start, Result(listOf(repoOverview)), End)

        given(vm.retrieveTrends()) {
            verifyForEmissions {
                items match uiEvents
            }
        }
    }


    private fun `trends for new request`() {
        whenever(api.retrieve()).thenAnswer { Observable.just(listOf(repoOverview)) }
    }

    private fun `when interact with cache`() {
        whenever(cache.store<String>(any(), any())).thenAnswer { Unit }
    }


    private val repoOverview = RepositoryOverview(
        id = 111,
        name = "trends",
        url = "URL_HERE",
        apiUrl = "URL_HERE",
        description = "a description here",
        starCount = 10,
        watcherCount = 10,
        forkCount = 10,
        imgUrl = "URL_HERE"
    )

}