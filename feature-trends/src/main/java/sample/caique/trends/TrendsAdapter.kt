package sample.caique.trends

import kotlinx.android.synthetic.main.item_trend.view.*
import sample.caique.common.BaseRecyclerViewAdapter
import sample.caique.domain.RepositoryOverview

class TrendsAdapter(private val onClick: (Int) -> Unit) :
    BaseRecyclerViewAdapter<RepositoryOverview>(layoutResId = R.layout.item_trend, bindView = { view, item ->
        with(item) {
            view.description.text = description
            view.name.text = name
            view.stars.text = starCount.toString()
            view.watchers.text = watcherCount.toString()
            view.forks.text = forkCount.toString()
            view.setOnClickListener { onClick(item.id) }
        }
    })