package sample.caique.trends

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.activity_trends.*
import org.kodein.di.direct
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import sample.caique.common.End
import sample.caique.common.Failed
import sample.caique.common.Result
import sample.caique.common.Start
import sample.caique.domain.RepositoryOverview
import sample.caique.injection.ProvidesInjector

class TrendsActivity : AppCompatActivity() {

    private val graph by lazy {
        val injector = (application as ProvidesInjector).injector

        injector.reconfigure {
            bind<FragmentActivity>() with provider { this@TrendsActivity }
        }

        injector.dependencies().direct
    }

    private val vm by lazy { graph.instance<TrendsViewModel>() }

    private val disposable by lazy {
        CompositeDisposable()
    }

    private val adapter by lazy {
        TrendsAdapter { id -> vm.proceedToDetail(id) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trends)
        with(listTrends) {
            layoutManager = LinearLayoutManager(this@TrendsActivity)
        }

        trends()
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }

    private fun trends() {
        disposable += vm.retrieveTrends()
            .subscribe(
                {
                    when (it) {
                        is Start -> start()
                        is Result -> setupView(it.value)
                        is Failed -> failed()
                        is End -> skeleton.visibility = View.GONE
                    }
                },
                { e -> e.printStackTrace() })
    }

    private fun failed() {
        errorState.visibility = View.VISIBLE
        errorState.retry { trends() }
    }

    private fun start() {
        skeleton.visibility = View.VISIBLE
        errorState.visibility = View.GONE

    }

    private fun setupView(result: List<RepositoryOverview>) {
        adapter.setItems(result)
        listTrends.adapter = adapter
    }
}
