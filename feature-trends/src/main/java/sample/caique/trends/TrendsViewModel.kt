package sample.caique.trends

import androidx.lifecycle.ViewModel
import sample.caique.common.UiState
import sample.caique.domain.Detail
import sample.caique.domain.Navigator
import sample.caique.domain.RepositoryOverview
import sample.caique.domain.Trends
import sample.caique.domain.service.CacheService
import sample.caique.domain.service.GithubService

class TrendsViewModel(
    private val api: GithubService,
    private val state: UiState<List<RepositoryOverview>>,
    private val cache: CacheService,
    private val navigate: Navigator
) : ViewModel() {

    fun retrieveTrends() =
        api.retrieve()
            .doOnNext { cache.store(Trends, it) }
            .compose(state)

    fun proceedToDetail(id: Int) {
        cache.store(Detail, id)
        navigate.proceedToDetail()
    }
}