package sample.caique.trends

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import sample.caique.common.UiState
import sample.caique.domain.Navigator
import sample.caique.domain.RepositoryOverview
import sample.caique.domain.service.CacheService
import sample.caique.domain.service.GithubService

class TrendsViewModelFactory(
    private val service: GithubService,
    private val state: UiState<List<RepositoryOverview>>,
    private val cache: CacheService,
    private val navigate: Navigator
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TrendsViewModel(service, state, cache, navigate) as T
    }
}