package sample.caique.trends.di

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import sample.caique.common.UiState
import sample.caique.scheduler.SchedulerTag
import sample.caique.trends.TrendsViewModel
import sample.caique.trends.TrendsViewModelFactory

val trends = Kodein.Module(name = "trends") {

    bind() from provider {

        val factory = TrendsViewModelFactory(
            service = instance(),
            state = UiState(uiScheduler = instance(SchedulerTag.uiScheduler)),
            navigate = instance(),
            cache = instance()
        )

        val activity: FragmentActivity = instance()

        ViewModelProviders.of(activity, factory)
            .get(TrendsViewModel::class.java)
    }
}