// Versions for project parameters and dependencies

object Versions {

    const val kotlin = "1.2.51"
    const val kotlinxSerialize = "0.6.0"

    const val androidGradleSupport = "3.1.4"

    const val testLogger = "1.4.0"
    const val gmsSupport = "4.1.0"

    const val titebilidade = "0.1.0"
    const val rxJava2 = "2.1.15"
    const val rxKotlin2 = "2.2.0"
    const val rxAndroid2 = "2.0.1"

    const val okhttp3 = "3.10.0"
    const val retrofit2 = "2.4.0"
    const val ktxConverter = "0.0.1"

    const val supportLibrary = "1.0.0"
    const val aac = "2.0.0-rc01"

    const val jUnit4 = "4.12"
    const val bursterTesting = "0.1.0"
    const val assertJ29 = "2.9.1"
    const val mockitoKT = "2.0.0-RC1"
    const val mockitoDexMaker2 = "2.19.0"
    const val androidJUnit = "1.0.2"

    const val roboletric = "4.0.1"
    const val barista2 = "2.5.0"
    const val rxEspressIdler = "0.9.0"
    const val androidTest = "1.1.0"

    const val kodeinDI = "5.2.0"
    const val slf4j = "1.7.25"

    const val fabric = "1.25.4"
    const val firebaseCrashlytics = "2.9.5"

    const val awaitility = "3.1.0"
}

object Dependencies {

    val kotlinStdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    val kotlinReflect = "org.jetbrains.kotlin:kotlin-reflect:${Versions.kotlin}"
    val kotlinSerialization = "org.jetbrains.kotlinx:kotlinx-serialization-runtime:${Versions.kotlinxSerialize}"

    val rxJava = "io.reactivex.rxjava2:rxjava:${Versions.rxJava2}"
    val rxKotlin = "io.reactivex.rxjava2:rxkotlin:${Versions.rxKotlin2}"
    val tite = "com.github.ubiratansoares:tite:${Versions.titebilidade}"

    val okhttp = "com.squareup.okhttp3:okhttp:${Versions.okhttp3}"
    val okhttpInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp3}"
    val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit2}"
    val retrofitRxAdapter = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit2}"
    val retrofitKTXConverter =
        "com.jakewharton.retrofit:retrofit2-kotlinx-serialization-converter:${Versions.ktxConverter}"

    val appCompat = "androidx.appcompat:appcompat:${Versions.supportLibrary}"
    val cardView = "androidx.cardview:cardview:${Versions.supportLibrary}"
    val recyclerView = "androidx.recyclerview:recyclerview:${Versions.supportLibrary}"
    val designSupport = "com.google.android.material:material:${Versions.supportLibrary}"

    val archComponentsViewModel = "androidx.lifecycle:lifecycle-viewmodel:${Versions.aac}"
    val archComponentsExtensions = "androidx.lifecycle:lifecycle-extensions:${Versions.aac}"
    val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid2}"

    val jUnit = "junit:junit:${Versions.jUnit4}"
    val assertJ = "org.assertj:assertj-core:${Versions.assertJ29}"
    val burster = "com.github.ubiratansoares:burster:${Versions.bursterTesting}"
    val mockitoKotlin = "com.nhaarman.mockitokotlin2:mockito-kotlin:${Versions.mockitoKT}"
    val mockitoDexMaker = "com.linkedin.dexmaker:dexmaker-mockito:${Versions.mockitoDexMaker2}"
    val androidTestRunner = "com.android.support.test:runner:${Versions.androidJUnit}"

    val espresso = "androidx.test:runner:${Versions.androidTest}"
    val roboletric = "org.robolectric:robolectric:${Versions.roboletric}"

    val rxIdler = "com.squareup.rx.idler:rx2-idler:${Versions.rxEspressIdler}"
    val barista = "com.schibsted.spain:barista:${Versions.barista2}"
    val mockWebServer = "com.squareup.okhttp3:mockwebserver:${Versions.okhttp3}"
    val kodein = "org.kodein.di:kodein-di-generic-jvm:${Versions.kodeinDI}"
    val kodeinConf = "org.kodein.di:kodein-di-conf-jvm:${Versions.kodeinDI}"
    val slf4jNoOp = "org.slf4j:slf4j-nop:${Versions.slf4j}"
    val crashlytics = "com.crashlytics.sdk.android:crashlytics:${Versions.firebaseCrashlytics}"

    val awaitility = "org.awaitility:awaitility:${Versions.awaitility}"

}

object BuildPlugins {

    val androidGradlePlugin = "com.android.tools.build:gradle:${Versions.androidGradleSupport}"
    val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    val kotlinxSerializiationPlugin =
        "org.jetbrains.kotlinx:kotlinx-gradle-serialization-plugin:${Versions.kotlinxSerialize}"
    val testLoggerPlugin = "com.adarshr:gradle-test-logger-plugin:${Versions.testLogger}"
    val gmsPlugin = "com.google.gms:google-services:${Versions.gmsSupport}"
    val firebaseCrashlytics = "io.fabric.tools:gradle:${Versions.fabric}"
}

object NetworkingDependencies {

    val main = listOf(
        Dependencies.okhttp,
        Dependencies.okhttpInterceptor,
        Dependencies.retrofit,
        Dependencies.retrofitRxAdapter,
        Dependencies.retrofitKTXConverter,
        Dependencies.kodein
    )

    val testing = listOf(
        Dependencies.slf4jNoOp,
        Dependencies.mockWebServer,
        Dependencies.tite
    )

}

object AndroidModule {

    val main = listOf(
        Dependencies.kotlinStdlib,
        Dependencies.rxJava,
        Dependencies.rxKotlin,
        Dependencies.appCompat,
        Dependencies.cardView,
        Dependencies.recyclerView,
        Dependencies.designSupport,
        Dependencies.archComponentsViewModel,
        Dependencies.archComponentsExtensions,
        Dependencies.rxAndroid,
        Dependencies.kodein,
        Dependencies.kodeinConf
    )

    val unitTesting = listOf(
        Dependencies.slf4jNoOp,
        Dependencies.jUnit,
        Dependencies.burster,
        Dependencies.assertJ,
        Dependencies.kotlinReflect,
        Dependencies.mockitoKotlin,
        Dependencies.tite

    )

    val androidTesting = listOf(
        Dependencies.slf4jNoOp,
        Dependencies.assertJ,
        Dependencies.androidTestRunner,
        Dependencies.barista,
        Dependencies.rxIdler,
        Dependencies.kotlinReflect,
        Dependencies.mockitoKotlin,
        Dependencies.mockWebServer,
        Dependencies.awaitility
    )
}

object StandaloneModule {

    val main = listOf(
        Dependencies.kotlinStdlib,
        Dependencies.rxJava,
        Dependencies.rxKotlin,
        Dependencies.kodein,
        Dependencies.kodeinConf
    )

    val unitTesting = listOf(
        Dependencies.jUnit,
        Dependencies.assertJ,
        Dependencies.burster,
        Dependencies.slf4jNoOp,
        Dependencies.mockitoKotlin,
        Dependencies.kotlinReflect,
        Dependencies.tite
    )

}
