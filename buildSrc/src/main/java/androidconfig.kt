object AndroidConfig {

    val applicationId = "sample.kanda.trends"

    val compileSdk = 28
    val minSdk = 21
    val targetSdk = compileSdk

    val buildToolsVersion = "27.0.3"

    val instrumentationTestRunner = "sample.kanda.trends.CustomTestRunner"
}