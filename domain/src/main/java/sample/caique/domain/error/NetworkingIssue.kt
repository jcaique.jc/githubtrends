package sample.caique.domain.error

sealed class NetworkingIssue : Throwable() {

    object InternetUnreachable : NetworkingIssue()
    object OperationTimeout : NetworkingIssue()
    object ConnectionSpike : NetworkingIssue()

}