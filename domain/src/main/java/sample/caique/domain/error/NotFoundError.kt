package sample.caique.domain.error

object NotFoundError : Throwable()