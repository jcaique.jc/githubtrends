package sample.caique.domain.error

sealed class GithubIntegrationError : Throwable() {

    override fun toString() = javaClass.simpleName

    object EntityNotFound : GithubIntegrationError() // aka REST 404
    object InternalServerError : GithubIntegrationError() // aka REST 5xy
    object UnexpectedResponse : GithubIntegrationError() // aka broken JSON contract
    object UnknownIssue : GithubIntegrationError()
}