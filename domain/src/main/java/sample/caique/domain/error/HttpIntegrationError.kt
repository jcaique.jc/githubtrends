package sample.caique.domain.error

sealed class HttpIntegrationError : Throwable() {

    object ClientIssue : HttpIntegrationError()
    object RemoteSystemIssue : HttpIntegrationError()
}