package sample.caique.domain

sealed class CacheKey

object Trends : CacheKey()
object Detail : CacheKey()