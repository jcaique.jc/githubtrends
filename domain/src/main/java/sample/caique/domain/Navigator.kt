package sample.caique.domain

interface Navigator {

    fun proceedToDetail()
}