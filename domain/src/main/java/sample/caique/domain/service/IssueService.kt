package sample.caique.domain.service

import io.reactivex.Observable
import sample.caique.domain.Issue

interface IssueService {
    fun retrieveIssues(fullName: String): Observable<List<Issue>>
}