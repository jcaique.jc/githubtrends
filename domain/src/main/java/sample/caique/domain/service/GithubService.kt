package sample.caique.domain.service

import io.reactivex.Observable
import sample.caique.domain.RepositoryOverview

interface GithubService {

    fun retrieve(): Observable<List<RepositoryOverview>>
}