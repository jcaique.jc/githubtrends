package sample.caique.domain.service

import sample.caique.domain.CacheKey

interface CacheService {

    fun clear()

    fun remove(key: CacheKey)

    fun <T> store(key: CacheKey, value: T)

    fun <T> retrieve(key: CacheKey): T?
}