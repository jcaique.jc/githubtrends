package sample.caique.di

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import sample.caique.networking.BuildRetrofit
import sample.caique.networking.ConfigRequest

val networkingModule = Kodein.Module(name = "networking") {

    bind<Retrofit>() with singleton {
        val configRequest: ConfigRequest = instance()
        BuildRetrofit(
            apiURL = configRequest.url,
            httpClient = instance()
        )
    }
}