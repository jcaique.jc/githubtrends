package sample.caique.networking

import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import kotlinx.serialization.SerializationException
import sample.caique.domain.error.GithubIntegrationError.UnexpectedResponse

class HandleSerializationError<T> : ObservableTransformer<T, T> {

    override fun apply(upstream: Observable<T>): Observable<T> {
        return upstream.onErrorResumeNext(this::handleIfSerializationError)
    }

    private fun handleIfSerializationError(error: Throwable): Observable<T> {
        error.printStackTrace()

        val mapped = when (error) {
            is SerializationException -> UnexpectedResponse
            else -> error
        }

        return Observable.error(mapped)
    }
}