package sample.caique.networking

data class ConfigRequest(
    val url: String
)