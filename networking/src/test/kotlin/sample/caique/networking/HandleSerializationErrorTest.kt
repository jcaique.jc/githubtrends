package sample.caique.networking

import io.reactivex.Observable
import kotlinx.serialization.SerializationException
import labs.dotanuki.tite.checks.broken
import labs.dotanuki.tite.given
import org.junit.Test
import sample.caique.domain.error.GithubIntegrationError.UnexpectedResponse

internal class HandleSerializationErrorTest {

    @Test
    fun `given a serialization error should map to UnexpectedException`() {
        val observable = Observable
            .error<Throwable>(SerializationException("error serialization"))
            .compose(HandleSerializationError())

        given(observable) {
            assertThatSequence {
                should be broken
            }

            verifyWhenError {
                fails byError UnexpectedResponse
            }
        }
    }

}