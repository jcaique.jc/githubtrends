package sample.caique.networking

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import labs.dotanuki.tite.checks.broken
import labs.dotanuki.tite.given
import org.junit.Test
import retrofit2.HttpException
import sample.caique.domain.error.HttpIntegrationError
import sample.caique.domain.error.HttpIntegrationError.ClientIssue
import java.util.*

private typealias StatusCode = Int

class HandleErrorByHttpStatusTest {

    private val httpException = mock<HttpException>()

    @Test
    fun `should map http exception to client issue`() {
        `with a error status code`((400..499).random())

        val observable = Observable
            .error<Throwable>(httpException)
            .compose(HandleErrorByHttpStatus())

        given(observable) {

            assertThatSequence {
                assertThatSequence {
                    should be broken
                }

                verifyWhenError {
                    fails byError ClientIssue
                }
            }
        }
    }

    @Test
    fun `should map http exception to Remote System issue`() {
        `with a error status code`((500..511).random())

        val observable = Observable
            .error<Throwable>(httpException)
            .compose(HandleErrorByHttpStatus())

        given(observable) {

            assertThatSequence {
                assertThatSequence {
                    should be broken
                }

                verifyWhenError {
                    fails byError HttpIntegrationError.RemoteSystemIssue
                }
            }
        }
    }

    private fun `with a error status code`(code: StatusCode) {
        whenever(httpException.code()).thenAnswer { code }
    }

    fun IntRange.random() =
        Random().nextInt((endInclusive + 1) - start) + start

}