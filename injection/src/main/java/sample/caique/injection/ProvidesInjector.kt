package sample.caique.injection

interface ProvidesInjector {

    val injector : Injector
}