package sample.caique.injection

import org.kodein.di.Kodein
import org.kodein.di.conf.ConfigurableKodein


/**
 * This class allows override the dependencies for tests =D
 */
class Injector(private val modules: List<Kodein.Module>) {

    private val originalGraph = Kodein {
        modules.forEach { import(it) }
    }

    private val configurableGraph = ConfigurableKodein(mutable = true).apply {
        addExtend(originalGraph, allowOverride = true)
    }

    fun dependencies() = configurableGraph

    fun reconfigure(block: Kodein.Builder.() -> Unit) {
        with(configurableGraph) {
            clear()
            addExtend(originalGraph, allowOverride = true)
            addConfig(block)
            this
        }
    }

}