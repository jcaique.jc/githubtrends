package sample.kanda.trends

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import sample.caique.details.DetailActivity
import sample.caique.domain.Navigator

class NavigatorManager(private val context: Context) : Navigator {

    override fun proceedToDetail() {
        val intent = Intent(context, DetailActivity::class.java)
        intent.flags = FLAG_ACTIVITY_NEW_TASK

        context.startActivity(intent)
    }
}