package sample.kanda.trends

import android.app.Application
import android.content.Context
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import sample.caique.cache.di.cacheModule
import sample.caique.details.di.details
import sample.caique.di.networkingModule
import sample.caique.domain.Navigator
import sample.caique.injection.Injector
import sample.caique.injection.ProvidesInjector
import sample.caique.networking.ConfigRequest
import sample.caique.scheduler.SchedulerTag
import sample.caique.service.di.serviceModule
import sample.caique.trends.di.trends

open class Application : Application(), ProvidesInjector {

    open fun configRequest() = ConfigRequest(url = "https://api.github.com")

    private val appModule = Kodein.Module(name = "application") {
        bind() from provider {
            this@Application as Context
        }

        bind() from provider {
            this@Application as Application
        }

        bind<Scheduler>(SchedulerTag.ioScheduler) with singleton {
            Schedulers.io()
        }

        bind<Scheduler>(SchedulerTag.uiScheduler) with singleton {
            AndroidSchedulers.mainThread()
        }

        bind<ConfigRequest>() with singleton {
            configRequest()
        }

        bind<Navigator>() with singleton {
            NavigatorManager(instance())
        }
    }

    override val injector = Injector(
        listOf(
            appModule,
            buildTypeModule,
            networkingModule,
            serviceModule,
            trends,
            details,
            cacheModule
        )
    )
}

