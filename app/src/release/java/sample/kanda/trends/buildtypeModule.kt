package sample.kanda.trends

import okhttp3.OkHttpClient
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton
import sample.caique.logger.Logger
import sample.caique.logger.SilentLogger
import java.util.concurrent.TimeUnit

val buildTypeModule = Kodein.Module(name = "buildType") {

    bind<Logger>() with singleton { SilentLogger }

    bind<OkHttpClient>() with singleton {
        OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .build()
    }
}