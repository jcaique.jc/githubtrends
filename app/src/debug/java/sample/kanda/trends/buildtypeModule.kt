package sample.kanda.trends

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton
import sample.caique.logger.ConsoleLogger
import sample.caique.logger.Logger
import java.util.concurrent.TimeUnit

val buildTypeModule = Kodein.Module(name = "buildType") {

    bind<Logger>() with singleton { ConsoleLogger }

    bind<OkHttpClient>() with singleton {
        OkHttpClient.Builder()
            .addInterceptor(httpLogger)
            .connectTimeout(30, TimeUnit.SECONDS)
            .build()
    }
}

private val httpLogger = HttpLoggingInterceptor().apply {
    level = HttpLoggingInterceptor.Level.BODY
}