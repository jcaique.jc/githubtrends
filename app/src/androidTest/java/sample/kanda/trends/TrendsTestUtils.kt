package sample.kanda.trends

const val repositories =
    """
    {
        "total_count": 24352,
        "incomplete_results": false,
        "items": [
            {
                  "full_name": "shadowsocks/shadowsocks-android",
                  "owner": {
                    "avatar_url": "https://avatars1.githubusercontent.com/u/3006190?v=4"
                  },
                  "html_url": "https://github.com/shadowsocks/shadowsocks-android",
                  "description": "A shadowsocks client for Android",
                  "url": "https://api.github.com/repos/shadowsocks/shadowsocks-android",
                  "stargazers_count": 19046,
                  "watchers_count": 19046,
                  "forks_count": 7907,
                  "open_issues_count": 10,
                  "forks": 7907,
                  "open_issues": 10,
                  "watchers": 19046,
                  "default_branch": "master",
                  "score": 72.74426
            }
          ]
    }
    """

const val error_json =

    """
            /
        """