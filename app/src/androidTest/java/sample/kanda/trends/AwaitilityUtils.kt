package sample.kanda.trends

import org.awaitility.Awaitility.await
import java.util.concurrent.TimeUnit

object AwaitilityUtils {

    fun awaitLongUntilAsserted(f: () -> Unit) = awaitUntilAsserted(15_000, f)

    fun awaitShortUntilAsserted(f: () -> Unit) = awaitUntilAsserted(5000, f)

    fun awaitUntilAsserted(timeoutMs: Long, f: () -> Unit) =
        await()
            .pollDelay(10, TimeUnit.MILLISECONDS)
            .pollInterval(100, TimeUnit.MILLISECONDS)
            .atMost(timeoutMs, TimeUnit.MILLISECONDS)
            .untilAsserted {
                try {
                    f()
                } catch (e: Throwable) {
                    e.printStackTrace()
                    throw AssertionError("ViewAssertion", e)
                }
            }
}