package sample.kanda.trends

import android.support.test.InstrumentationRegistry
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import okhttp3.mockwebserver.MockResponse
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import sample.caique.trends.TrendsActivity

@RunWith(AndroidJUnit4::class)
@LargeTest
class TrendsActivityTest {

    @get:Rule
    private val activityTestRule =
        ActivityTestRule(TrendsActivity::class.java, true, false)

    @get:Rule
    var mockWebServerRule = MockWebServerRule()

    @Before
    fun setup() {
        val app = InstrumentationRegistry.getTargetContext().applicationContext as TestApplication
        app.url = mockWebServerRule.server.url("/").toString()
    }

    @Test
    fun should_show_list_view() {
        mockWebServerRule.server.enqueue(MockResponse().setBody(repositories))
        activityTestRule.launchActivity(null)

        withRobot {
            toolbarIsVisible()
            listTrendsIsVisible()
        }
    }

    @Test
    fun should_show_screen_error() {
        mockWebServerRule.server.enqueue(MockResponse().setBody(error_json))
        activityTestRule.launchActivity(null)

        withRobot {
            errorScreenIsVisible()
        }
    }
}

