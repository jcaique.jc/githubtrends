package sample.kanda.trends

import sample.caique.networking.ConfigRequest

class TestApplication : Application() {

    open lateinit var url: String

    override fun configRequest(): ConfigRequest {
        return ConfigRequest(url)
    }
}