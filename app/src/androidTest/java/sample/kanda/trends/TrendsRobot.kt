package sample.kanda.trends

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import sample.kanda.trends.AwaitilityUtils.awaitLongUntilAsserted
import sample.kanda.trends.AwaitilityUtils.awaitShortUntilAsserted

/**
 * Robot pattern to espresso test
 * https://medium.com/android-bits/espresso-robot-pattern-in-kotlin-fc820ce250f7
 */

object TrendsRobot {

    fun toolbarIsVisible() = awaitShortUntilAsserted {
        onView(withId(R.id.toolbar))
            .check(matches(isDisplayed()))
    }

    fun listTrendsIsVisible() = awaitLongUntilAsserted {
        onView(withId(R.id.listTrends))
            .check(matches(isDisplayed()))
    }

    fun errorScreenIsVisible() = awaitLongUntilAsserted {
        onView(withId(R.id.errorView)).check(matches(isDisplayed()))
    }
}

fun withRobot(body: TrendsRobot.() -> Unit) {
    body(TrendsRobot)
}