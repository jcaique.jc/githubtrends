package sample.kanda.trends

import android.app.Application
import android.content.Context
import android.support.test.runner.AndroidJUnitRunner

/**
 * Just a custom runner to allows override the base url
 * https://github.com/chiuki/mockwebserver-demo
 **/

class CustomTestRunner : AndroidJUnitRunner() {
    @Throws(InstantiationException::class, IllegalAccessException::class, ClassNotFoundException::class)
    override fun newApplication(
        cl: ClassLoader, className: String, context: Context
    ): Application {
        return super.newApplication(cl, TestApplication::class.java.name, context)
    }
}